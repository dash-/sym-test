<?php

namespace UsersBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use UsersBundle\Entity\User;
use UsersBundle\EventListener\UserListener;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\Event;

class TestFormController extends Controller
{
    public function indexAction(Request $request)
    {

        $dispatcher = new EventDispatcher();
        $listener = new UserListener();
        

        $registration = new User();
        $registration->setBirthday(new \DateTime());
        $registration->setIp($this->container->get('request')->getClientIp());

        $form = $this->createFormBuilder($registration)
            ->add('name', 'text', array ('attr' => array ('class' => 'form-control form-field')))
            ->add('surname', 'text', array ('attr' => array ('class' => 'form-control form-field')))
            ->add('email', 'email', array ('attr' => array ('class' => 'form-control form-field')))
            ->add('birthday', 'birthday', array ('attr' => array ('class' => 'form-field')))
            ->add('save', 'submit', array('label' => 'Register', 'attr' => array('class' => 'btn btn-primary registerButton')))
            ->getForm();

         $form->handleRequest($request);

        if ($form->isValid()) 
        {
            $dispatcher->addListener('postPersist', array($listener, 'onDatabaseUpdate'));

            $em = $this->getDoctrine()->getManager();
            $em->persist($registration);
            $em->flush();
            $dispatcher->dispatch('postPersist');

            $message = \Swift_Message::newInstance()
                ->setSubject('You are registered')
                ->setFrom('test@symfony.dash.ba')
                ->setTo($registration->getEmail())
                ->setBody(
                    $this->renderView(
                        'UsersBundle:MailTemplates:registered.html.php',
                        array('name' => $registration->getName())
                    ),
                    'text/html'
                );

            $this->get('mailer')->send($message);
            
        }

        return $this->render('UsersBundle:TestForm:index.html.php', array('form' => $form->createView()));
    }

}