<?php

namespace UsersBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use UsersBundle\Entity\User;
use UsersBundle\Entity\RegistrationIP;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\HttpFoundation\RedirectResponse;

class UserListener
{
    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $entityManager = $args->getEntityManager();

        if ($entity instanceof User) 
        {
            $registrationIP = new RegistrationIP();
            $registrationIP->setUserId($entity->getId());
            $registrationIP->setIp($entity->getIp());

            $entityManager->persist($registrationIP);
            $entityManager->flush();

            
        }
    }


    public function onDatabaseUpdate()
    {
        echo 'Sending mail...';

    }
    
}