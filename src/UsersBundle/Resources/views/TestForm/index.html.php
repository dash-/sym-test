<?php $view->extend('UsersBundle:Layout:layout.html.php') ?>

<?php $view['slots']->start('body') ?>
<div class="well form-container">
    <?= $view['form']->start($form) ?>
	<?= $view['form']->widget($form) ?>
	<?= $view['form']->end($form) ?>
</div>
<?php $view['slots']->stop() ?>
