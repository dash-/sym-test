<!DOCTYPE html>
<html>
    <head>
    	<script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.11.2.min.js"></script>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="<?= $view['assets']->getUrl('bundles/testform/css/main.css') ?>" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?php $view['slots']->output('title', 'Symfony2 Test Form') ?></title>
    </head>
    <body>
        <div id="content">
            <?php $view['slots']->output('body') ?>
        </div>
    </body>
</html>