<?php

use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;

$collection = new RouteCollection();

$collection->add('users_homepage', new Route('/hello/{name}', array(
    '_controller' => 'UsersBundle:Default:index',
)));

$collection->add('test_form', new Route('/', array(
    '_controller' => 'UsersBundle:TestForm:index',
)));

$collection->add('updated_notification', new Route('/notification', array(
    '_controller' => 'UsersBundle:TestForm:index',
)));

return $collection;
